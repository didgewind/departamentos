package profe.ms.departamentosRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import profe.ms.empleadosweb.services.EmpleadosService;
import profe.ms.empleadosweb.services.EmpleadosServiceStatic;

@SpringBootApplication
public class DepartamentosServer {

	public static void main(String[] args) {
		SpringApplication.run(DepartamentosServer.class, args);
	}

	@Bean
	public EmpleadosService empleadosService() {
		return new EmpleadosServiceStatic();
	}
	
}
